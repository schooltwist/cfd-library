<?php

namespace SchoolTwist\Cfd\Library;



class CfdInt extends \SchoolTwist\Cfd\Core\CfdBase
{
    public int $Value;

    static function Value_Validates($candidateValue): \SchoolTwist\Validations\Returns\DtoValid
    {
        return new \SchoolTwist\Validations\Returns\DtoValid(
            ['isValid' => \SchoolTwist\Validations\Inspect\Numeric::IsInteger($candidateValue)
            ]);
    }
//    public static function upConvertType_elseNull(string $propertyName, $dangerousValue)
//    {
//        $incomingType = gettype($dangerousValue);
//        if ($incomingType == 'integer') {
//            try {
//                $descendantName = get_called_class();
//                $dangerousValue_new = new $descendantName(['Value' => $dangerousValue]);
//            } catch (\Throwable $e) {
//                return null;
//            }
//            return $dangerousValue_new;
//
//        } else {
//            return null;
//        }
//    }
}