<?php

namespace SchoolTwist\Cfd\Library;


abstract class CfdIntBounded extends CfdInt
{
    // public $Value;
    CONST MIN = 0;
    CONST MAX = 1;

    static function Value_Validates($candidateValue): \SchoolTwist\Validations\Returns\DtoValid
    {
        $dtoValidation = parent::Value_Validates($candidateValue);
        if (!$dtoValidation->isValid) {
            return $dtoValidation;
        }
        if ($candidateValue < static::MIN || $candidateValue > static::MAX) { // Hmm, bounded int seems reasonable for the future
            return new \SchoolTwist\Validations\Returns\DtoValid([
                'isValid' => false,
                'enumReason' => 'ValueOutOfBounds',
                "The int($candidateValue) must be between '" . static::MIN . "' and '" . static::MAX . "'"
            ]);
        }
        return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true]);
    }
}