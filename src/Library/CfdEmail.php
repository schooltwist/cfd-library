<?php

namespace SchoolTwist\Cfd\Library;

class CfdEmail extends CfdShortString
{
    // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address/44317754
    static function  Value_Validates($candidateValue) : \SchoolTwist\Validations\Returns\DtoValid {

        $dto = parent::Value_Validates($candidateValue);
        if (!$dto->isValid) {
            return $dto;
        }

        if (!filter_var($candidateValue, FILTER_VALIDATE_EMAIL)) { //http://stackoverflow.com/questions/12026842/how-to-validate-an-email-address-in-php
                return new \SchoolTwist\Validations\Returns\DtoValid([
                    'isValid'=>false,
                    'enumReason'=>'notAnEmail',
                    'message'=>"'{$candidateValue}' doesn't look like a real email address.  We need your real email for communicating with you.",
                    ]
                );
            }
//            else {
//                if (\EtDeployment::IsProduction() || \EtDeployment::IsStaging() || (\EtDeployment::IsDev() && \EtDeployment::isConnectedToTheInternet())) {
//                    if (!checkdnsrr(substr(strrchr($candidateValue, "@"), 1), 'MX')) { //http://www.bitrepository.com/how-to-extract-domain-name-from-an-e-mail-address-string.html
//                        return new DtoValid([
//                            'isValid'=>false,
//                            'enumReason'=>'notAnEmailThatPassesCheckdnsrr',
//                            'message'=>"'{$candidateValue}' doesn't look like a real domain in your email address. Expecting something like 'yahoo.com' or 'gmail.com'. We need your real email for communicating with you.",
//                            ]
//                        );
//                    }
//                }
//
//            }
        return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>true]);
    }
}