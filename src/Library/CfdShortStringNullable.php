<?php

namespace SchoolTwist\Cfd\Library;


class CfdShortStringNullable extends CfdShortStringBase
{
    use ValueAsStringNullableImplementation;

    const MaxStringLength = 256;
}