<?php

namespace SchoolTwist\Cfd\Library;




Class CfdbOneToTen extends CfdIntBounded implements \EtFramework19\Cfdb\Core\CfdbLeaf
{
    CONST MIN = 1;
    CONST MAX = 10;
}



class CfdbUniqueSound extends CfdbShortText implements \EtFramework19\Cfdb\Core\CfdbLeaf
{
    public static function getSqlCreateColumn(): \EtFramework19\Cfdb\Core\CfdSqlCreateCol
    {
        $asr = parent::getSqlCreateColumn()->toArray();
        $asr['IsUnique'] = true;
        return new \EtFramework19\Cfdb\Core\CfdSqlCreateCol($asr);
    }
}


class CfdbQuipText extends \EtFramework19\Cfdb\Library\CfdbShortText
{
    const MaxStringLength = 141;
}

class CfdbMood extends DtoCfd implements \EtFramework19\Cfdb\Core\CfdbBranch
{
    use \EtFramework19\Cfdb\Core\CfdbBranchTrait;

    /** @var EtFramework19\Dto\Library\CfdbOneToTen */
    public $Multiplier;

    /** @var EtFramework19\Cfdb\Library\CfdbShortText */
    public $Description;

    /** @var EtFramework19\Dto\Library\CfdbUniqueSound */
    public $UniqueSound;
}

class CftblTweet extends \EtFramework19\Cfdb\Core\CfdbUuidTbl
{
    public static $internalVersionAsInteger = 144;

    /** @var EtFramework19\Dto\Library\CfdbQuipText */
    public $Quip;

    /** @var EtFramework19\Cfdb\Library\CfdbWhole */
    public $NumVisits;
    public static function wakeUp()
    {
    }
}


CftblTweet::__constructStatically(); // Needed for versioning

