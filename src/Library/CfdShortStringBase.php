<?php

namespace SchoolTwist\Cfd\Library;


abstract class CfdShortStringBase extends CfdShortTextBase
{

    const MaxStringLength = 256;
}