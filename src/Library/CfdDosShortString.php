<?php

namespace SchoolTwist\Cfd\Library;


class CfdDosShortString extends CfdShortString
{
     const MaxStringLength = 8;
}