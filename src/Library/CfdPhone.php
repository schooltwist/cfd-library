<?php

namespace SchoolTwist\Cfd\Library;

class CfdPhone extends CfdVeryShortString
{

    static function Value_Validates($candidateValue): \SchoolTwist\Validations\Returns\DtoValid
        {
             $justTheDigits = preg_replace("/[^0-9,.]/", "", $candidateValue); //http://stackoverflow.com/questions/4949279/remove-non-numeric-characters-plus-comma-and-period-from-a-string
            if (strlen($justTheDigits) < 10) {
                #$asrArrErrorMsgHtml['phone'][] = GenerateHtmlError("'{$candidateValue}' doesn't look like a real phone number.  I'm looking for 10, or more, digits.");
                return new \SchoolTwist\Validations\Returns\DtoValid([
                            'isValid'=>false,
                            'enumReason'=>'notPhoneFormat',
                            'message'=>"'{$candidateValue}' doesn't look like a real phone number.  I'm looking for 10, or more, digits.",
                            ]
                        );
            }
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>true]);
        }
}