<?php
namespace SchoolTwist\Cfd\Library;


abstract class CfdShortTextBase extends \SchoolTwist\Cfd\Core\CfdBase  implements UiSchemaReallyShouldNotBeHere, ValueInterface
{
    #public string $Value;
    #use ValueAsStringImplementation;
    # -or- Pick one in you concrete class
    #use ValueAsStringNullableImplementation;

    const MaxStringLength = 4096;

    static function  Value_Validates($candidateValue) : \SchoolTwist\Validations\Returns\DtoValid
    {
        if (is_null($candidateValue) && static::isValueNullable()) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>true, 'enumReason'=>'null==null', 'message'=>'Null given and allowed']);
        }
        if (empty(trim($candidateValue))) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>false, 'enumReason'=>'ReducesToEmpty', 'message'=>'Reduces to Empty']);
        } elseif (strlen($candidateValue.'') > static::MaxStringLength) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid'=>false, 'enumReason'=>'TooLong', 'message'=>'Length is more than the allowed '.static::MaxStringLength.' characters.']);
        } else {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true]);
        }
    }

    public static function getUiSchema() : string
    {
        return 'string_input';
    }

    // 2/20' How sure are we that this is a good idea/
    public function __toString() : string
    {
        if (is_null($this->Value)) {
            return '';
        } else {
            return $this->Value;
        }
    }

    public static function from_string(string $valueAsString, bool $assumeValidValue): self {
        if (!$assumeValidValue) {
            assert(static::Value_Validates($valueAsString)->isValid);
        }

        $myName = get_called_class();
        $newMe = new $myName(['Value'=>$valueAsString]);
        return $newMe;

    }

}
