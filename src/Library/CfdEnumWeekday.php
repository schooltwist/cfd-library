<?php
namespace SchoolTwist\Cfd\Library;
class CfdEnumWeekday extends \SchoolTwist\Cfd\Lib\CfdEnumValue {
    /** @var string */
    public string $Value;
    static public array $_ArrEnumValuePossibilities = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
}