<?php
namespace SchoolTwist\Cfd\Library;


class CfdShortText extends CfdShortTextBase  implements UiSchemaReallyShouldNotBeHere, ValueInterface
{

    use ValueAsStringImplementation;

    const MaxStringLength = 4096;


}
