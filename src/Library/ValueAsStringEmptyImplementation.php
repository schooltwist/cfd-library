<?php
namespace SchoolTwist\Cfd\Library;

trait ValueAsStringEmptyImplementation  {
    public string $Value = '';
    public static function isValueNullable() : bool {
        return false;
    }
}