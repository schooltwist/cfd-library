<?php

namespace SchoolTwist\Cfd\Library;


class CfdShortString extends CfdShortStringBase
{
    use ValueAsStringImplementation;

    const MaxStringLength = 256;
}