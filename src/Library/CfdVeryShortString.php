<?php

namespace SchoolTwist\Cfd\Library;


class CfdVeryShortString extends CfdShortString
{
     const MaxStringLength = 128;
}