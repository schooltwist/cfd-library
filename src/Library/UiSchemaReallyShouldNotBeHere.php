<?php
namespace SchoolTwist\Cfd\Library;


interface UiSchemaReallyShouldNotBeHere
{
    public static function getUiSchema() : string;
}