<?php
namespace SchoolTwist\Cfd\Library;



class CfdTime extends \SchoolTwist\Cfd\Core\CfdBase {

    public string $Value;

    public static function Value_Validates($maybeValidValue) : \SchoolTwist\Validations\Returns\DtoValid {
        $format = 'H:i:s';
        $t = date($format,strtotime($maybeValidValue));

        if ($maybeValidValue == $t) {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => true]);
        } else {
            return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => false, 'enumReason'=>'NotRoundtripping','message'=>"$t !=$maybeValidValue Please pass data as exaclty $format " ]);
        }
    }

    public static function now_asString() : string
    {
        return date('H:i:s');//3:11:25
    }
}




