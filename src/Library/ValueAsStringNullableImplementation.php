<?php
namespace SchoolTwist\Cfd\Library;

trait ValueAsStringNullableImplementation  {
    public ?string $Value;
    public static function isValueNullable() : bool {
        return true;
    }

    public static function from_NULL(?string $valueAsString, bool $assumeValidValue): self {
        assert(is_null($valueAsString));
        assert(static::isValueNullable());

        if (!$assumeValidValue) {
            assert(static::Value_Validates($valueAsString)->isValid);
        }

        $myName = get_called_class();
        $newMe = new $myName(['Value'=>null]);
        return $newMe;

    }
}