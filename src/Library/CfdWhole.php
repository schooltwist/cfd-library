<?php

namespace SchoolTwist\Cfd\Library;

class CfdWhole extends CfdInt
{

    public int $Value;

    static function Value_Validates($candidateValue): \SchoolTwist\Validations\Returns\DtoValid
    {
        return new \SchoolTwist\Validations\Returns\DtoValid(['isValid' => \SchoolTwist\Validations\Inspect\Numeric::IsWhole($candidateValue)]);
    }


}