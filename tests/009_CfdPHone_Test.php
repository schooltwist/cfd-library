<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



final class TestDtoCfd_Phone extends TestCase
{

    function test_CfdPhone_bad()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "a", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', null, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "(617)398060", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', 1234567, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', 123456789, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "1234567", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "123456789", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
    }

    function test_CfdPhone_good()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "61739806091", null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "1234567890", null);
        $this->assertTrue($dtoValid->isValid ,"ok " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "(617)39806091", null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);
      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "(617)3980601", null);
        $this->assertTrue($dtoValid->isValid, "ok - we're just counting digits" . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdPhone::preValidateProperty('Value', "123456789012334", null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

    }


}