<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



class CfdRealIntPercentage extends \SchoolTwist\Cfd\Library\CfdIntBounded
{
    CONST MIN = 0;
    CONST MAX = 100;
}

final class TestDtoCfd_Bounder extends TestCase
{

    function test_CfdInt_bad()
    {
        try {
            $dtoValid = new CfdRealIntPercentage(['Value'=>-1]);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, "ok" . __LINE__);
        }
        try {
            $dtoValid = new CfdRealIntPercentage(['Value'=>-1.1]);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, "ok" . __LINE__);
        }
        try {
            $dtoValid = new CfdRealIntPercentage(['Value'=>100]);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, "ok" . __LINE__);
        }
        try {
            $dtoValid = new CfdRealIntPercentage(['Value'=>-1]);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, "ok" . __LINE__);
        }
        try {
            $dtoValid = new CfdRealIntPercentage(['Value'=>-1]);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, "ok" . __LINE__);
        }

        try {
            $dtoValid = new CfdRealIntPercentage(['Value'=>"5"]);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, "ok" . __LINE__);
        }

    }

    function test_CfdInt_good()
    {
         $cfd = new CfdRealIntPercentage(['Value'=>5]);
        $this->assertTrue($cfd->Value == 5, "ok " . __LINE__);

         $cfd = new CfdRealIntPercentage(['Value'=>0]);
        $this->assertTrue($cfd->Value == 0, "ok " . __LINE__);

         $cfd = new CfdRealIntPercentage(['Value'=>100]);
        $this->assertTrue($cfd->Value == 100, "ok " . __LINE__);

         $cfd = new CfdRealIntPercentage(['Value'=>99]);
        $this->assertTrue($cfd->Value == 99, "ok " . __LINE__);


    }


}