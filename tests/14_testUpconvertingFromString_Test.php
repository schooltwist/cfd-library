<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class Profile2 extends \SchoolTwist\Cfd\Core\CfdBase
{
    public \SchoolTwist\Cfd\Library\CfdShortString $FirstName;

    public \SchoolTwist\Cfd\Library\CfdShortString $LastName;
}

final class TestDtoCfd_UpCasting extends TestCase
{
    function test_hw()
    {
        $fName = new \SchoolTwist\Cfd\Library\CfdShortString(['Value'=>'JJ']);
        $lName = new \SchoolTwist\Cfd\Library\CfdShortString(['Value'=>'Jones']);
        $dtoValid = Profile2::preValidateProperty('FirstName', $fName, null);
         $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);
        $dtoValid = Profile2::preValidateProperty('FirstName', $lName, null);
         $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

         $cfd = new Profile2(['FirstName'=>$fName, 'LastName'=>$lName]);
         $this->assertTrue($cfd->FirstName->Value == "JJ" ,'ok'. __LINE__);
         $this->assertTrue($cfd->LastName->Value == "Jones",'ok' . __LINE__);
    }

    function test_CfdDosShortString_bad()
    {
      $dtoValid = Profile2::preValidateProperty('FirstName', 88, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

    }
  function test_CfdDosShortString_good()
    {
      $dtoValid = Profile2::preValidateProperty('FirstName', 'JJ', null);
      if (!$dtoValid->isValid) {
          print_r($dtoValid);
          exit;
      }
        $this->assertTrue($dtoValid->isValid, "ok - shows we can upconvert strings " . __LINE__);

    }


}