<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



final class TestDtoCfd_ShortStringe extends TestCase
{

    function test_CfdDosShortString_bad()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', "abcdefghi", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', null, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', 1234567, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', 123456789, null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', "123456789", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);
    }

    function test_CfdDosShortString_good()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', "abcdefgh", null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdDosShortString::preValidateProperty('Value', "a", null);
        $this->assertTrue($dtoValid->isValid ,"ok " . __LINE__);



    }


}