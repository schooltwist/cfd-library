<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



final class TestDtoCfd_ShortString2e extends TestCase
{

    function test_CfdDosShortString_bad()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdEmail::preValidateProperty('Value', "jj", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdEmail::preValidateProperty('Value', 'jj@rohrer', null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdEmail::preValidateProperty('Value', 1234567, null);

    }

    function test_CfdDosShortString_good()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdEmail::preValidateProperty('Value', "jj@rohrer.org", null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdEmail::preValidateProperty('Value', "jj.spam@rohrer.org", null);
        $this->assertTrue($dtoValid->isValid ,"ok " . __LINE__);




    }


}