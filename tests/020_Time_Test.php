<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;



final class TestDtoCfd_ShortString33e extends TestCase
{

    function test_CfdDosShortString_bad()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdTime::preValidateProperty('Value', "26:00", null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);


      $dtoValid = \SchoolTwist\Cfd\Library\CfdTime::preValidateProperty('Value', '2:00pm', null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdTime::preValidateProperty('Value', '2:00:00', null);
        $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

    }

    function test_CfdDosShortString_good()
    {
      $dtoValid = \SchoolTwist\Cfd\Library\CfdTime::preValidateProperty('Value', "11:00:23", null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

      $dtoValid = \SchoolTwist\Cfd\Library\CfdTime::preValidateProperty('Value', "23:04:00", null);
        $this->assertTrue($dtoValid->isValid ,"ok " . __LINE__);



    }


}