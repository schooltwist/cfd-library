<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class OtherWhole extends \SchoolTwist\Cfd\Core\CfdBase
{

    public \SchoolTwist\Cfd\Library\CfdWhole $MyWhole;
}

final class TestDtoCfd_UpCasting_fromIntoWhole extends TestCase
{
    function test_hw()
    {
        $cfd = new \SchoolTwist\Cfd\Library\CfdWhole(['Value'=>5]);
        $this->assertTrue($cfd->Value == 5 ,'ok'. __LINE__);

                $cfd = new \SchoolTwist\Cfd\Library\CfdWhole(['Value'=>5]);
        $this->assertTrue($cfd->Value == 5 ,'ok'. __LINE__);

                $cfd = new \SchoolTwist\Cfd\Library\CfdWhole(['Value'=>0]);
        $this->assertTrue($cfd->Value == 0 ,'ok'. __LINE__);

        try {
             $cfd = new \SchoolTwist\Cfd\Library\CfdWhole(['Value'=>-1]);
        $this->assertTrue(false ,'never'. __LINE__);
        } catch (Throwable $e) {
               $this->assertTrue(true ,'ok'. __LINE__);
        }

        $asrData = [
            'MyWhole'=> new \SchoolTwist\Cfd\Library\CfdWhole(['Value'=>88])
            ];
         $cfd = new OtherWhole($asrData);
        $this->assertTrue($cfd->MyWhole->Value == 88 ,'ok'. __LINE__);


    }

    function test_CfdDosShortString_bad()
    {
       $dtoValid = \SchoolTwist\Cfd\Library\CfdWhole::preValidateProperty('Value', 1.2, null);
        $this->assertFalse($dtoValid->isValid, " " . __LINE__);

        $this->assertTrue(true, " " . __LINE__);


        // convert from string?? - nope
        try {
            $asrData = [
                'MyWhole' => '88'
            ];
            $cfd = new OtherWhole($asrData);
            $this->assertTrue(false, 'ok' . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, 'ok' . __LINE__);
        }

        // Does it still do testing via Validates?

        try {
            $asrData = [
                'MyWhole' => 90.2
            ];
            $cfd = new OtherWhole($asrData);
            $this->assertTrue(false, 'ok' . __LINE__);
        } catch (Throwable $e) {
            $this->assertTrue(true, 'ok' . __LINE__);
        }


    }
  function test_CfdDosShortString_good()
    {

      $asrData = [
            'MyWhole'=> 88
            ];
         $cfd = new OtherWhole($asrData);
        $this->assertTrue($cfd->MyWhole->Value == 88 ,'ok'. __LINE__);

    }


}