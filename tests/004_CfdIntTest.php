<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;




final class TestDtoCfd2a extends TestCase
{

    function test_CfdInt_bad()
    {
            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', "a", null);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', "1", null);
            $this->assertTrue($dtoValid->isValid, "Tis find.  String to int is an exception if we validate it as int. " . __LINE__);

            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', "0", null);
        $this->assertTrue($dtoValid->isValid, "Tis find.  String to int is an exception if we validate it as int. " . __LINE__);

            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', null, null);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', null, null);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', 2.4, null);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

            $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', "-1.2", null);
            $this->assertFalse($dtoValid->isValid, "Should not get this far " . __LINE__);

    }

    function test_CfdInt_good()
    {
        $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', 0, null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

        $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', 1, null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

        $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', 2, null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);

        $dtoValid = \SchoolTwist\Cfd\Library\CfdInt::preValidateProperty('Value', -1, null);
        $this->assertTrue($dtoValid->isValid, "ok " . __LINE__);


    }


}